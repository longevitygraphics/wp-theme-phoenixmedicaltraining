<?php

global $lg_tinymce_custom;
$selector          = 'h1,h2,h3,h4,h5,h6';
$lg_tinymce_custom = array(
	'title' => 'Custom',
	'items' => array(
		array(
			'title'   => 'Fancy Title',
			'selector'  => $selector,
			'classes' => 'fancy-title',
		),
		array(
			'title'   => 'Fancy Title Center',
			'selector'  => $selector,
			'classes' => 'fancy-title fancy-title--center',
		),
		array(
			'title'   => 'List with icon',
			'selector' => 'ul',
			'classes' => 'fancy-list',
		),
	)
);
