<?php

class LGFunctions {

	private static $instance = null;

	private function __construct() {
		add_action( 'wp_content_top', array( $this, 'featured_banner_top' ) );
		add_filter( 'wp_nav_menu_items', array( $this, 'main_nav_items' ), 10, 2 );

	}

	public static function getInstance() {
		if ( self::$instance == null ) {
			self::$instance = new lgFunctions();
		}

		return self::$instance;
	}

	function featured_banner_top() {
		ob_start(); ?>
		<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>
		<?php echo ob_get_clean();
	}

	function main_nav_items( $items, $args ) {
		//lg_write_log( $args );
		//pr($items);
		if ( $args->menu->slug == 'top-nav' ) {
			$items .= '<li class="d-none d-lg-inline-block nav-phone menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link text-white btn btn-primary" href="mailto:' . do_shortcode("[lg-email]") . '">Email Us</a></li>';
		}

		return $items;
	}
}

add_action( 'admin_menu', 'remove_links_menu' );
function remove_links_menu() {
     remove_submenu_page('admin.php', 'lg_client_instruction');
}

lgFunctions::getInstance();




